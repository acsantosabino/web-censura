import { FormControl, InputLabel, MenuItem, Select } from "@material-ui/core";
import React, { useContext, useEffect, useState } from "react";
import { GravadorContext } from "../../Contexts/GravdorContext";
import styles from "./Codificacao.module.css"

function Codificacao() {

    const {deviceId, setDeviceId, bitRate, setBitRate} = useContext(GravadorContext)
    const [device_list, setDeviceList] = useState([] as MediaDeviceInfo[])

    function load_devices() {
        navigator.mediaDevices.enumerateDevices()
            .then(function (devices) {
                setDeviceList(devices.filter(device => device.kind.includes("audio")))
            })
            .catch(function (err) {
                console.log(err.name + ": " + err.message);
            })
    }

    useEffect(() => {
        load_devices()
    }, [])

    return (
        <div className="container">
            <h2>Codificação (MPeg 1 Layer III)</h2>
            <FormControl className={styles.selectDevice}>
                <InputLabel id="device-label">Dispositivo de Entrada</InputLabel>
                <Select
                    labelId="device-label"
                    id="device"
                    fullWidth
                    value={deviceId}
                    onChange={(event) => setDeviceId(typeof event.target.value === 'string' 
                    ? event.target.value : '')}
                >
                    <MenuItem value={""}>Device</MenuItem>
                    {
                        device_list.map(function (device, index) {
                           return <MenuItem key={index} value={device.deviceId}>{device.label}</MenuItem>
                        })
                    }
                </Select>
            </FormControl>
            <div className={styles.selectGroup}>
                <FormControl>
                    <InputLabel id="frequencia-label">Frequencia</InputLabel>
                    <Select
                        labelId="frequencia-label"
                        id="frequencia"
                        fullWidth
                        value={0}
                    >
                        <MenuItem value={0}>22050</MenuItem>
                    </Select>
                </FormControl>
                <FormControl>
                    <InputLabel id="modo-label">Modo</InputLabel>
                    <Select
                        labelId="modo-label"
                        id="modo"
                        fullWidth
                        value={0}
                    >
                        <MenuItem value={0}>Stereo</MenuItem>
                    </Select>
                </FormControl>
                <FormControl>
                    <InputLabel id="bitrate-label">BitRate (Kbps)</InputLabel>
                    <Select
                        labelId="bitrate-label"
                        id="bitrate"
                        fullWidth
                        value={bitRate}
                        onChange={(event) => setBitRate(Number(event.target.value))}
                    >
                        <MenuItem value={64}>64</MenuItem>
                        <MenuItem value={128}>128</MenuItem>
                        <MenuItem value={256}>256</MenuItem>
                    </Select>
                </FormControl>
                <FormControl>
                    <InputLabel id="qualidade-label">VBR Qualidade</InputLabel>
                    <Select
                        labelId="qualidade-label"
                        id="qualidade"
                        fullWidth
                        value={0}
                    >
                        <MenuItem value={0}>0 (Alta)</MenuItem>
                    </Select>
                </FormControl>
            </div>
        </div>
    )
}

export default Codificacao;