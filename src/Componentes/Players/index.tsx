import { useContext } from "react";
import { GravadorContext } from "../../Contexts/GravdorContext";

function Players() {
  const { slices } = useContext(GravadorContext);
  return (
    <div>
      {slices.map((url, index) => (
        <audio key={index} src={url} controls={true} />
      ))}
    </div>
  );
}

export default Players;