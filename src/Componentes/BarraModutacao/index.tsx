import React, { useContext, useEffect, useState } from "react";
import { GravadorContext } from "../../Contexts/GravdorContext";
import styles from "./BarraModutacao.module.css"

function BarraModutacao() {
    const [nivel, setNivel] = useState([0]);
    const { getNivel } = useContext(GravadorContext);

    const bufferLength = nivel.length
    const nivelMean = Math.max(...nivel) //(nivel.reduce((a, b)=> a+b, 0)/bufferLength) || 0
    const nbarras = Math.round(55 * nivelMean / 255);

    useEffect(() => {
        const id = setTimeout(() => {
            const newNivel = getNivel();
            // console.log("New nivel:", nbarras)
            setNivel(newNivel)
        }, 50)

        return () => clearTimeout(id)
    }, [nivel])

    function getColor(n: number) {
        if (n >= 50) return styles.red
        else if (n >= 40) return styles.yellow
        else return styles.green
    }

    return (
        <div className={styles.barrasContainer}>
            <div>
                {
                    Array.from({length: nbarras}, (value, key) => key).map(n => <div className={[styles.barra, getColor(n)].join(' ')}/> )
                }
            </div>
            <div>
                {
                    Array.from({length: nbarras}, (value, key) => key).map(n => <div className={[styles.barra, getColor(n)].join(' ')}/> )
                }
            </div>
        </div>
    )
}

export default BarraModutacao;