import { InputAdornment, TextField } from "@material-ui/core";
import { Folder } from "@material-ui/icons";
import React, { useContext } from "react";
import { GravadorContext } from "../../Contexts/GravdorContext";
import styles from "./Armazenamento.module.css";

function Armazenamento() {
  const {timeSlice, dias, bitRate, setTimeSlice, setDias} = useContext(GravadorContext)

  const timeSliceMin = timeSlice / 60;

  const espaco = (dias * 24 * 3600 * bitRate) / 1024;
  
  return (
    <div className={"container "+styles.gridTemplate}>
      <h2>Armazenamento</h2>
      <div className={[styles.campo, styles.diaArea].join(' ')}>
        <TextField
          type="number"
          label="Arquivar por:"
          InputProps={{ inputProps: { max: 99, min: 0 } }}
          value={dias}
          onChange={(event) => setDias(Number(event.target.value))}
        />
        <span>Dia(s)</span>
      </div>
      <div className={[styles.campo, styles.minArea].join(' ')}>
        <TextField
          type="number"
          label="Gravar Bloco de:"
          value={timeSliceMin}
          onChange={(event) => setTimeSlice(60 * Number(event.target.value) )}
          InputProps={{ inputProps: { max: 99, min: 0 } }}
        />
        <span>Minuto(s)</span>
      </div>
      <div className={[styles.campo, styles.espaceArea].join(' ')}>
        <TextField label="Espaço Necessário" value={espaco} disabled />
        <span>MByte(s)</span>
      </div>

      <div className={styles.pastaArea}>
        <TextField
          label="Diretório Base"
          fullWidth
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Folder/>
              </InputAdornment>
            )
          }}
        />
      </div>
    </div>
  );
}

export default Armazenamento;
