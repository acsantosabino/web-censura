import React, { useEffect, useState } from 'react';
import styles from './Relogio.module.css';
import moment from 'moment';
import 'moment/locale/pt-br';
import 'react-clock/dist/Clock.css';
import Clock from 'react-clock';

function Relogio() {
    
    const [time, setTime] = useState(moment());
    
    useEffect(() => {
        const interval = setInterval(
        () => setTime(moment()),
        1000
        );
    
        return () => {
        clearInterval(interval);
        }
    }, []);

    return (
        <div className={styles.relogioContainer}>
            <h2>Relógio</h2>
            <div className={styles.relogioTexto}>
                <span>{time.format("HH:mm:ss")}</span>
                <span>{time.format("dddd LL")}</span>
            </div>

                
            <div className={styles.relogioAnalogico}>
                <Clock value={time.toDate()} size={60}/>
            </div>
        </div>
    )
}

export default Relogio