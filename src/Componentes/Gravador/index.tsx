import React, { useContext, useEffect, useState } from 'react';
import styles from './Gravador.module.css';
import moment from 'moment';
import 'moment/locale/pt-br';
import 'moment-duration-format';
import BarraModutacao from '../BarraModutacao';
import { GravadorContext } from '../../Contexts/GravdorContext';

function Gravador() {
    const {isRecording} = useContext(GravadorContext)
    const [startedTime, setStartedTime] = useState(moment())

    useEffect(() => {
        setStartedTime(moment())
    }, [isRecording])

    const tempo = moment.duration(moment().diff(startedTime), "milliseconds").format("hh:mm:ss,SSS", {trim: false})

    return (
        <div className={styles.gravadorContainer}>
            <h2>Gravador</h2>
            <div className={styles.nivelEntradaCotainer}>
                <h3>Nivel de Entrada</h3>
                <BarraModutacao/>
            </div>
            <div className={styles.diretorioCotainer}>
                <h3>Salvando em</h3>
                <span>D:\Censura_CBN_TO\20210822</span>
            </div>
            <div className={styles.arquivoCotainer}>
                <h3>Arquivo</h3>
                <span>123739.mp3</span>
            </div>
            <div className={styles.statusCotainer}>
                <div>
                    <h3>Status</h3>
                    {
                        isRecording ? 
                        <span>GRAVANDO</span> : 
                        <span>Parado</span>
                    }
                </div>
                <div>
                    <h3>Tempo</h3>
                    {
                        isRecording ?
                        <span>{tempo}</span> :
                        <span>00 : 00 : 00</span>
                    }
                </div>
            </div>
        </div>
    )
}

export default Gravador;