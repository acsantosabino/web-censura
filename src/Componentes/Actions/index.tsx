import { Close, FiberManualRecord, Stop } from "@material-ui/icons";
import React, { useContext } from "react";
import { GravadorContext } from "../../Contexts/GravdorContext";
import styles from './Actions.module.css'

function Actions() {
    const { isRecording, startRecord, stopRecord} = useContext(GravadorContext)
    return (
        <div className={styles.actionContainer}>
            <button className={styles.actionButton}
                onClick={startRecord}
                disabled={isRecording}
            >
                <span>Gravar</span>
                <FiberManualRecord/>
            </button>
            <button className={styles.actionButton}
                onClick={stopRecord}
                disabled={!isRecording}
            >
                <span>Parar</span>
                <Stop/>
            </button>
            <button className={styles.actionButton}>
                <span>Sair</span>
                <Close/>
            </button>
        </div>
    )
}

export default Actions;