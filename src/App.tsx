import React from 'react';
import './App.css';
import Gravador from './Componentes/Gravador'
import Relogio from './Componentes/Relogio';
import Armazenamento from './Componentes/Armazenamento';
import Codificacao from './Componentes/Codificacao';
import Actions from './Componentes/Actions';
import { GravadorProvider } from './Contexts/GravdorContext';
import Players from './Componentes/Players';

function App() {
  return (
    <div className="App">
      <GravadorProvider>
        <div>
          <Gravador/>
        </div>
        <div>
          <Relogio/>
          <Armazenamento />
          <Codificacao />
          <Actions />
        </div>
        <Players/>
      </GravadorProvider>
    </div>
  );
}

export default App;
