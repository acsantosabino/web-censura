import { createContext, Dispatch, ReactNode, SetStateAction, useEffect, useState } from "react";

interface GravadorContextData {
  getNivel: () => number[];
  startRecord: () => void;
  stopRecord: () => void;
  setDeviceId: Dispatch<SetStateAction<string>>;
  setTimeSlice: Dispatch<SetStateAction<number>>;
  setDias: Dispatch<SetStateAction<number>>;
  setBitRate: Dispatch<SetStateAction<number>>;
  bitRate: number;
  dias: number;
  timeSlice: number;
  deviceId: string;
  isRecording: boolean;
  slices: string[];
}

interface GravadorProviderProps {
  children: ReactNode;
}

export const GravadorContext = createContext({} as GravadorContextData);

function isEmpty(obj: any): boolean {
  return (
    obj && // 👈 null and undefined check
    Object.keys(obj).length === 0 &&
    obj.constructor === Object
  );
}

export function GravadorProvider({ children }: GravadorProviderProps) {
  const [mediaRecorder, setMediaRecorder] = useState({} as MediaRecorder);
  const [stream, setStream] = useState({} as MediaStream);
  const [isRecording, setIsRecording] = useState(false);
  const [audioCtx, setAudioCtx] = useState({} as AudioContext);
  const [analyser, setAnalyser] = useState({} as AnalyserNode);
  const [chunks, setChunks] = useState([] as Blob[]);
  const [timeSlice, setTimeSlice] = useState(60);
  const [slices, setSlices] = useState([] as string[]);
  const [deviceId, setDeviceId] = useState("default");
  const [dias, setDias] = useState(7);
  const [bitRate, setBitRate] = useState(64);

  // let chunks = [] as Blob[];

  function initMediaRecorder() {
    navigator.mediaDevices
      .getUserMedia({ audio: { deviceId: deviceId } })
      .then((newStream) => setStream(newStream))
      .catch((err) => console.log(err));
  }

  function startRecord() {
    mediaRecorder.start(1000);
    console.log(mediaRecorder.state);
    console.log(`recorder started ${timeSlice}s`);
    setIsRecording(true);
  }

  useEffect(() => {
    console.log("slices:", slices);
  }, [slices]);

  function stopRecord() {
    mediaRecorder.stop();
    console.log(mediaRecorder.state);
    console.log("recorder stopped");
    setIsRecording(false);
    saveChunks()
  }

  function initAnalyser() {
    const source = audioCtx.createMediaStreamSource(stream);
    const newAnalyser = audioCtx.createAnalyser();
    newAnalyser.fftSize = 128;
    newAnalyser.maxDecibels = -10;
    newAnalyser.minDecibels = -90;
    source.connect(newAnalyser);
    setAnalyser(newAnalyser);
  }

  function getNivel(): number[] {
    if (!isEmpty(analyser)) {
      const bufferLength = analyser.frequencyBinCount;
      const dataArray = new Uint8Array(bufferLength);
      analyser.getByteFrequencyData(dataArray)
      return Array.from(dataArray);
    }
    return Array.from({length: 128}, () => 0);
  }

  useEffect(() => {
    initMediaRecorder();
  }, [deviceId]);

  useEffect(() => {
    if (!isEmpty(audioCtx)) {
      initAnalyser();
    }
  }, [audioCtx]);

  useEffect(() => {
    if (!isEmpty(stream)) {
      console.log("getUserMedia supported.");
      console.log(stream);
      console.log("getAudioTracks()");
      console.log(stream.getAudioTracks());
      const newMediaRecorder = new MediaRecorder(stream);
      newMediaRecorder.ondataavailable = (e) =>
        setChunks(oldChunks => [...oldChunks, e.data]);

      setMediaRecorder(newMediaRecorder);
      if (isEmpty(audioCtx)) {
        setAudioCtx(new AudioContext());
      }
    }
  }, [stream]);

  function saveChunks() {
    const blob = new Blob(chunks, { type: "audio/ogg; codecs=opus" });
    setSlices(oldSlices => [...oldSlices, window.URL.createObjectURL(blob)]);
    setChunks(chunks.slice(timeSlice));
  }

  useEffect(() => {
    console.log("nchunks:", chunks.length);
    if (chunks.length >= timeSlice) {
      saveChunks()
    }

  }, [chunks]);

  return (
    <GravadorContext.Provider
      value={{
        getNivel,
        startRecord,
        stopRecord,
        setDeviceId,
        setTimeSlice,
        setDias,
        setBitRate,
        bitRate,
        dias,
        timeSlice,
        deviceId,
        isRecording,
        slices,
      }}
    >
      {children}
    </GravadorContext.Provider>
  );
}
